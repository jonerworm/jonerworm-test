#!/bin/sh
# firstlogin.sh
#
# base.worm.sh first script to configure SSH access to enable remote admin,
# create initial user, install SSH public key, enable passwordless sudo
#

set -x # enables debugging

ERROR_LOG=~/"firstlogin.log"
SSHKEY_USER_CLR='firstlogin'
SSHKEY_USER='x'`echo "$SSHKEY_USER_CLR" | sha256sum | cut -b1-7`
SSHKEY_FILE="${SSHKEY_USER_CLR}_id_ed25519.key.pub"
SSHKEY_REPO='jonerworm-test'
SSHKEY_REPO_USER='jonerworm'
SSHKEY_REPO_BRANCH='master'
SSHKEY_REPO_URI="https://gitlab.com/${SSHKEY_REPO_USER}/${SSHKEY_REPO}/-/raw/${SSHKEY_REPO_BRANCH}/${SSHKEY_FILE}"

( \
	
	test which yum >/dev/null 2>&1 && INSTALL_TMP='yum -y install' || INSTALL_TMP='apt-get -y install'
	
	test which sudo >/dev/null 2>&1 || eval "${INSTALL_TMP}" sudo || exit $?
	
	test which curl >/dev/null 2>&1 || eval "${INSTALL_TMP}" curl || exit $?
	
	id "$SSHKEY_USER" >/dev/null 2>&1 || useradd -c "$SSHKEY_USER" -m "$SSHKEY_USER" || exit $?
	
	passwd --lock $SSHKEY_USER
	
	mkdir -p /etc/sudoers.d >/dev/null 2>&1
	
	chmod 0750 /etc/sudoers.d
	
	SUDOERS="/etc/sudoers.d/$SSHKEY_USER"
	
	echo "$SSHKEY_USER ALL=(ALL:ALL) NOPASSWD: ALL" >"$SUDOERS"
	
	chmod 0640 "$SUDOERS"
	
	su -s /bin/sh -c 'echo user `whoami` will try to run: sudo id:; sudo id' $SSHKEY_USER
	
	eval SSH_DIR=~$SSHKEY_USER/.ssh
	
	mkdir "$SSH_DIR" >/dev/null 2>&1
	
	chmod 0700 "$SSH_DIR"
	
	SSH_CONFIG="$SSH_DIR/config"
	
	eval >"$SSH_CONFIG"
	
	chmod 0400 "$SSH_CONFIG"
	
	cat >"$SSH_CONFIG" <<_EoF_
Host *
   ForwardAgent no
   ForwardX11 no
   ForwardX11Trusted yes
   RhostsRSAAuthentication no
   RSAAuthentication yes
   PasswordAuthentication no ############## 
   HostbasedAuthentication no
   GSSAPIAuthentication no
   GSSAPIDelegateCredentials no
   GSSAPIKeyExchange no
   GSSAPITrustDNS no
   BatchMode no
   CheckHostIP no #####################
   AddressFamily any
   ConnectTimeout 0
   StrictHostKeyChecking no  ######################
   IdentityFile ~/.ssh/identity
   IdentityFile ~/.ssh/id_rsa
   IdentityFile ~/.ssh/id_dsa
   #   Port 22
   #   Protocol 2,1
   #   Cipher 3des
   #   Ciphers aes128-ctr,aes192-ctr,aes256-ctr,arcfour256,arcfour128,aes128-cbc,3des-cbc
   #   MACs hmac-md5,hmac-sha1,umac-64@openssh.com,hmac-ripemd160
   #   EscapeChar ~
   Tunnel no
   #   TunnelDevice any:any
   #   PermitLocalCommand no
   #   VisualHostKey no
   #   ProxyCommand ssh -q -W %h:%p gateway.example.com
   SendEnv LANG LC_*
   HashKnownHosts yes
   ############    GSSAPIAuthentication yes
   ############    GSSAPIDelegateCredentials no
   
_EoF_
	
	AUTHKEYS="$SSH_DIR/authorized_keys"
	
	eval >"$AUTHKEYS"
	
	chmod 0640 "$AUTHKEYS"
	
	chown -R ${SSHKEY_USER}.${SSHKEY_USER} "$SSH_DIR"
	
	curl "${SSHKEY_REPO_URI}" >"${AUTHKEYS}" || exit $?
	
) 2>&1 | tee "$ERROR_LOG"

exit $?